# question-bank-manage

题库

##启动项目
执行 `pnpm dev` 命令，
```
$ pnpm dev
ready - App listening at http://127.0.0.1:8000
event - compiled successfully in 1121 ms (388 modules)
event - MFSU compiled successfully in 1308 ms (875 modules)
```

##部署发布
执行 `pnpm build` 命令，
```
> umi build
event - compiled successfully in 1179 ms (567 modules)
event - build index.html
```
产物默认会生成到 ./dist 
```
./dist
├── index.html
├── umi.css
└── umi.js
```
完成构建后，就可以把 dist 目录部署到服务器上了。

`@umijs/max` 模板项目，更多功能参考 [Umi Max 简介](https://next.umijs.org/zh-CN/docs/max/introduce)

[ProComponents](https://procomponents.ant.design/)  [umijs.org](https://umijs.org/)
