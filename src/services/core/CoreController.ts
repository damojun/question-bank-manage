import { request } from '@umijs/max';

export async function queryClassesList(
  params: {
    // query
    /** keyword */
    keyword?: string;
    /** current */
    current?: number;
    /** pageSize */
    pageSize?: number;
  },
  options?: { [key: string]: any },
) {
  return request<API.Result_PageInfo_UserInfo__>('/api/manage/Classes/all', {
    method: 'GET',
    params: {
      ...params,
    },
    ...(options || {}),
  });
}

export async function addClasses(
  body?: API.ClassesInfo,
  options?: { [key: string]: any },
) {
  return request<API.Result_UserInfo_>('/api/manage/Classes/create', {
    method: 'POST',
    data: body,
    ...(options || {}),
  });
}

export async function getUserDetail(
  params: {
    // path
    /** userId */
    userId?: string;
  },
  options?: { [key: string]: any },
) {
  const { userId: param0 } = params;
  return request<API.Result_UserInfo_>(`/api/v1/user/${param0}`, {
    method: 'GET',
    params: { ...params },
    ...(options || {}),
  });
}

export async function modifyUser(
  params: {
    // path
    /** userId */
    userId?: string;
  },
  body?: API.UserInfoVO,
  options?: { [key: string]: any },
) {
  const { userId: param0 } = params;
  return request<API.Result_UserInfo_>(`/api/v1/user/${param0}`, {
    method: 'PUT',
    params: { ...params },
    data: body,
    ...(options || {}),
  });
}

export async function deleteClasses(
  body?: API.ClassesInfo,
  options?: { [key: string]: any },
) {
  return request<API.Result_UserInfo_>('/api/manage/Classes/del', {
    method: 'POST',
    data: body,
    ...(options || {}),
  });
}

export async function querySubjectList(
  params: {
    // query
    /** keyword */
    keyword?: string;
    /** current */
    current?: number;
    /** pageSize */
    pageSize?: number;
  },
  options?: { [key: string]: any },
) {
  return request<API.Result_PageInfo_UserInfo__>('/api/manage/Subject/all', {
    method: 'GET',
    params: {
      ...params,
    },
    ...(options || {}),
  });
}
export async function addSubject(
  body?: API.ClassesInfo,
  options?: { [key: string]: any },
) {
  return request<API.Result_UserInfo_>('/api/manage/Subject/create', {
    method: 'POST',
    data: body,
    ...(options || {}),
  });
}
export async function deleteSubject(
  body?: API.ClassesInfo,
  options?: { [key: string]: any },
) {
  return request<API.Result_UserInfo_>('/api/manage/Subject/del', {
    method: 'POST',
    data: body,
    ...(options || {}),
  });
}
