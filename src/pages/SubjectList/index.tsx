import services from '@/services/core';
import {
  ActionType,
  FooterToolbar,
  PageContainer,
  ProDescriptions,
  ProDescriptionsItemProps,
  ProTable,
} from '@ant-design/pro-components';
import { Button, Divider, Drawer, message } from 'antd';
import React, { useRef, useState, useEffect } from 'react';
import CreateForm from './components/CreateForm';

const { addSubject, querySubjectList, deleteSubject, queryClassesList } = services.CoreController;

/**
 * 添加节点
 * @param fields
 */
const handleAdd = async (fields: API.SubjectInfo, isEdit?: boolean) => {
  const hide = message.loading(isEdit?'正在修改':'正在添加');
  try {
    await addSubject({ ...fields });
    hide();
    message.success(isEdit?'修改成功':'添加成功');
    return true;
  } catch (error) {
    hide();
    message.error(isEdit?'修改失败请重试':'添加失败请重试！');
    return false;
  }
};



/**
 *  删除节点
 * @param selectedRows
 */
const handleRemove = async (selectedRows: API.UserInfo[]) => {
  const hide = message.loading('正在删除');
  if (!selectedRows) return true;
  try {
    await deleteSubject({
      id: selectedRows.find((row) => row.id)?.id || undefined,
    });
    hide();
    message.success('删除成功，即将刷新');
    return true;
  } catch (error) {
    hide();
    message.error('删除失败，请重试');
    return false;
  }
};
const getClassesList = async () => {
  const { data } = await queryClassesList({})
  
  const classesEnum = data.list.reduce((obj: any, {id, title}) => {
    obj[id] = {text: title }
    return obj
  }, {})
  console.log(data, classesEnum)
  return classesEnum
}

const TableList: React.FC<unknown> = () => {
  const [createModalVisible, handleModalVisible] = useState<boolean>(false);
  const [editableKeys, setEditableRowKeys] = useState<React.Key[]>([]);
  
  const actionRef = useRef<ActionType>();
  const [row, setRow] = useState<API.UserInfo>();
  const [selectedRowsState, setSelectedRows] = useState<API.UserInfo[]>([]);
  const [classesEnum, setClassesEnum] = useState({});
  useEffect(() => {
    (async()=>{
      const classesEnumData = await getClassesList()
      setClassesEnum(classesEnumData)
    })()
  }, []);

  
  const columns: ProDescriptionsItemProps<API.UserInfo>[] = [
    {
      title: '科目名称',
      dataIndex: 'title',
      tip: '科目名称',
      formItemProps: {
        rules: [
          {
            required: true,
            message: '名称为必填项',
          },
        ],
      },
    },
    {
      title: '状态',
      dataIndex: 'status',
      filters: true,
      onFilter: true,
      valueType: 'select',
      formItemProps: {
        rules: [
          {
            required: true,
            message: '状态为必填项',
          },
        ],
      },
      valueEnum: {
        1: {
          text: '启用',
          status: 'Success',
        },
        0: {
          text: '禁用',
          status: 'Error',
        },
      },
    },
    {
      title: '科目类别',
      dataIndex: 'classes_id',
      filters: true,
      onFilter: true,
      valueType: 'select',
      formItemProps: {
        rules: [
          {
            required: true,
            message: '科目类别为必填项',
          },
        ],
      },
      valueEnum: classesEnum,
    },
    
    {
      title: '操作',
      dataIndex: 'option',
      valueType: 'option',
      render: (_, record, index, action) => (
        <>
          <a
            onClick={() => {
              action?.startEditable?.(record.id);
            }}
          >
            修改
          </a>
          <Divider type="vertical" />
          <a
            onClick={async() => {
              const success = await handleRemove([record]);
              if (success && actionRef.current) {
                actionRef.current.reload();
              }
            }}
          >
            删除
          </a>
        </>
      ),
    },
  ];
  return (
    <PageContainer
      header={{
        title: '科目列表',
      }}
    >
      <ProTable<API.SubjectInfo>
        headerTitle=""
        actionRef={actionRef}
        rowKey="id"
        search={{
          labelWidth: 120,
        }}

        editable={{
          type: 'multiple',
          editableKeys,
          onSave: async (rowKey, data: any, row: any) => {
            const {id, title, status, classes_id} = data
            // await waitTime(2000);
            const success = await handleAdd({id, title, status, classes_id}, true)
            if (success) {
            
            }else{
              data.title = row.title
              data.status = row.status
            }
          },
          onDelete: async (rowKey, data: any)=>{ await handleRemove(data) },
          onChange: setEditableRowKeys,
        }}
        toolBarRender={() => [
          <Button
            key="0"
            type="primary"
            onClick={() =>{
              handleModalVisible(true)
            }}
          >
            新建
          </Button>,
        ]}
        request={async (params, sorter, filter) => {
          const { data, success } = await querySubjectList({
            ...params,
            sorter,
            filter,
          });
          const subjectList = (data?.list || []).map((m:any) => {
            m.classes_id = m.classes_id.toString()
            return m
          })
          return {
            data: subjectList,
            success,
          };
        }}
        columns={columns}
        rowSelection={{
          onChange: (_, selectedRows) => setSelectedRows(selectedRows),
        }}
      />
      {selectedRowsState?.length > 0 && (
        <FooterToolbar
          extra={
            <div>
              已选择{' '}
              <a style={{ fontWeight: 600 }}>{selectedRowsState.length}</a>{' '}
              项&nbsp;&nbsp;
            </div>
          }
        >
          <Button
            onClick={async () => {
              await handleRemove(selectedRowsState);
              setSelectedRows([]);
              actionRef.current?.reloadAndRest?.();
            }}
          >
            批量删除
          </Button>
        </FooterToolbar>
      )}
      <CreateForm
        onCancel={() => handleModalVisible(false)}
        modalVisible={createModalVisible}
      >
        <ProTable<API.UserInfo, API.UserInfo>
          onSubmit={async (value) => {
            const success = await handleAdd(value);
            if (success) {
              handleModalVisible(false);
              if (actionRef.current) {
                actionRef.current.reload();
              }
            }
          }}
          rowKey="id"
          type="form"
          columns={columns}
        />
      </CreateForm>


      <Drawer
        width={600}
        visible={!!row}
        onClose={() => {
          setRow(undefined);
        }}
        closable={false}
      >
        {row?.name && (
          <ProDescriptions<API.UserInfo>
            column={2}
            title={row?.name}
            request={async () => ({
              data: row || {},
            })}
            params={{
              id: row?.name,
            }}
            columns={columns}
          />
        )}
      </Drawer>
    </PageContainer>
  );
};

export default TableList;
