import { defineConfig } from '@umijs/max';

export default defineConfig({
  antd: {},
  access: {},
  model: {},
  initialState: {},
  request: {},
  layout: {
    title: '题库',
  },
  proxy: {
    '/api': {
      'target': 'http://learn.bintang.cloud/api/',
      'changeOrigin': true,
      'pathRewrite': { '^/api' : '' },
    },
  },
  routes: [
    {
      path: '/',
      redirect: '/home',
    },
    {
      name: '首页',
      path: '/home',
      component: './Home',
    },
  
    {
      name: '科目列表',
      path: '/SubjectList',
      component: './SubjectList',
    },
    {
        name: '科目分类',
        path: '/classesType',
        component: './ClassesType',
    },
  ],
  npmClient: 'pnpm',
});

